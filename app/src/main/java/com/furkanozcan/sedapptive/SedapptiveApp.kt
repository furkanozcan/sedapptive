package com.furkanozcan.sedapptive

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SedapptiveApp: Application()
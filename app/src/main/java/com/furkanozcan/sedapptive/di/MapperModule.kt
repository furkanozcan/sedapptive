package com.furkanozcan.sedapptive.di

import com.furkanozcan.sedapptive.ui.mapper.HomeMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object MapperModule {

    @Provides
    @Singleton
    fun provideHomeMapper() = HomeMapper()
}
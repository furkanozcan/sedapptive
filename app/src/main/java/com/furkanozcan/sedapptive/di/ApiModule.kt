package com.furkanozcan.sedapptive.di

import com.furkanozcan.sedapptive.data.remote.BlobService
import com.furkanozcan.sedapptive.data.remote.HomeRemoteDataSource
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
object ApiModule {

    @Provides
    @Singleton
    @Named(BLOB_BASE_URL)
    fun provideBaseUrl(): String = "https://jsonblob.com/api/jsonBlob/"

    @Provides
    @Singleton
    fun provideLoggingInterceptor(): HttpLoggingInterceptor =
        HttpLoggingInterceptor().apply { level = HttpLoggingInterceptor.Level.BODY }

    @Provides
    @Singleton
    fun provideOkHttpClient(
        loggingInterceptor: HttpLoggingInterceptor,
    ): OkHttpClient =
        with(OkHttpClient.Builder()) {
            addInterceptor(loggingInterceptor).build()
        }

    @Provides
    @Singleton
    fun provideRetrofit(
        @Named(BLOB_BASE_URL)
        baseUrl: String,
        client: OkHttpClient
    ): Retrofit =
        with(Retrofit.Builder()) {
            baseUrl(baseUrl)
            client(client)
            addConverterFactory(MoshiConverterFactory.create())
            build()
        }

    @Provides
    @Singleton
    fun provideBlobService(retrofit: Retrofit): BlobService =
        retrofit.create(BlobService::class.java)

    @Provides
    @Singleton
    fun provideHomeRemoteDataSource(blobService: BlobService): HomeRemoteDataSource {
        return HomeRemoteDataSource(blobService)
    }


    private const val BLOB_BASE_URL = "blob_base_url"
}
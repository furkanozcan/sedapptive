package com.furkanozcan.sedapptive.di

import com.furkanozcan.sedapptive.data.HomeRepository
import com.furkanozcan.sedapptive.data.remote.HomeRemoteDataSource
import com.furkanozcan.sedapptive.ui.mapper.HomeMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RepositoryModule {

    @Provides
    @Singleton
    fun provideHomeRepository(
        homeRemoteDataSource: HomeRemoteDataSource,
        homeMapper: HomeMapper
    ) = HomeRepository(
        homeRemoteDataSource,
        homeMapper
    )
}
package com.furkanozcan.sedapptive.utils

import android.text.format.DateFormat
import java.util.*

const val EMPTY_STRING = ""

private const val DATE_TIME_FORMAT = "dd/MM/yyyy, EEE"

fun String.hasUpperCase() = this != this.lowercase(Locale.getDefault())

fun String.hasNumber(): Boolean = this.matches(Regex(".*\\d+.*"))

fun String?.toHumanReadableDate(): String {
    this?.let { timestamp ->
        val calendar = Calendar.getInstance(Locale.getDefault()).apply {
            timeInMillis = timestamp.toLong() * 1000L
        }

        return DateFormat.format(DATE_TIME_FORMAT, calendar).toString()
    }

    return EMPTY_STRING
}
package com.furkanozcan.sedapptive.utils

fun Int.isZero() = this == 0
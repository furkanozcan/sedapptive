package com.furkanozcan.sedapptive.utils

fun Boolean?.orFalse() = this ?: false
package com.furkanozcan.sedapptive.utils

import com.furkanozcan.sedapptive.data.Resource
import retrofit2.HttpException
import retrofit2.Response

private const val NO_DATA_MESSAGE = "An error occurred while loading content."

val <T : Any> Response<T>.asResource: Resource<T>
    get() {
        return if (this.isSuccessful) {
            body()?.let { body ->
                Resource.Success(body)
            } ?: Resource.Error(NO_DATA_MESSAGE)
        } else {
            Resource.Error(HttpException(this).message())
        }
    }
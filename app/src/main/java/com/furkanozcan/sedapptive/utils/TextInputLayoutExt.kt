package com.furkanozcan.sedapptive.utils

import com.google.android.material.textfield.TextInputLayout

fun TextInputLayout.clearError() {
    this.error = EMPTY_STRING
    this.isErrorEnabled = false
}
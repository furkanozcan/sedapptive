package com.furkanozcan.sedapptive.data.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HomeResponse(
    val isBannerEnabled: Boolean? = null,
    val meditations: List<MeditationResponse>? = null,
    val stories: List<StoryResponse>? = null
)
package com.furkanozcan.sedapptive.data.local

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

val Context.dataStore: DataStore<Preferences> by preferencesDataStore(name = "app_storage")

@Singleton
class DataStorage @Inject constructor(
    @ApplicationContext context: Context
) {

    private val dataStore = context.dataStore

    suspend fun setUserName(userName: String) {
        dataStore.edit {
            it[USER_NAME_INFO] = userName
        }
    }

    fun getUserName(): Flow<String> = dataStore.data.map { preferences ->
        preferences[USER_NAME_INFO].orEmpty()
    }

    companion object {
        val USER_NAME_INFO = stringPreferencesKey("user_name")
    }
}
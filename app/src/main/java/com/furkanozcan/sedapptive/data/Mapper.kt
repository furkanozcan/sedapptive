package com.furkanozcan.sedapptive.data

interface Mapper<IN, OUT> {
    fun map(input: IN): OUT
}
package com.furkanozcan.sedapptive.data.remote

import com.furkanozcan.sedapptive.data.model.HomeResponse
import retrofit2.Response
import retrofit2.http.GET

interface BlobService {
    @GET("a07152f5-775c-11eb-a533-c90b9d55001f")
    suspend fun getHomeData(): Response<HomeResponse>
}
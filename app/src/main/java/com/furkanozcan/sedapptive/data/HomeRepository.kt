package com.furkanozcan.sedapptive.data

import com.furkanozcan.sedapptive.data.remote.HomeRemoteDataSource
import com.furkanozcan.sedapptive.ui.mapper.HomeMapper
import com.furkanozcan.sedapptive.ui.model.HomeUiModel
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class HomeRepository @Inject constructor(
    private val homeRemoteDataSource: HomeRemoteDataSource,
    private val homeMapper: HomeMapper
) {
    suspend fun getHomeUiData(): Flow<Resource<HomeUiModel>> = flow {
        emit(Resource.Loading<HomeUiModel>())

        when(val result = homeRemoteDataSource.getHomeResponseData()) {
            is Resource.Success -> {
                emit(Resource.Success(homeMapper.map(result.data)))
            }
            else -> {
                emit(Resource.Error<HomeUiModel>(result.message.orEmpty()))
            }
        }
    }
}
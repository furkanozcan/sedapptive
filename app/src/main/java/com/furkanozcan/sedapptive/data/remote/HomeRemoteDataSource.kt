package com.furkanozcan.sedapptive.data.remote

import com.furkanozcan.sedapptive.data.Resource
import com.furkanozcan.sedapptive.data.model.HomeResponse
import com.furkanozcan.sedapptive.utils.asResource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class HomeRemoteDataSource @Inject constructor(
    private val service: BlobService
) {
    suspend fun getHomeResponseData(): Resource<HomeResponse> = withContext(Dispatchers.IO) {
        return@withContext service.getHomeData().asResource
    }
}
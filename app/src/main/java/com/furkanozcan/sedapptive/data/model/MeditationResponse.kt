package com.furkanozcan.sedapptive.data.model

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class MeditationResponse(
    val title: String? = null,
    @Json(name = "subtitle")
    val subTitle: String? = null,
    val image: ImageResponse? = null,
    val releaseDate: String? = null,
    val content: String? = null
)
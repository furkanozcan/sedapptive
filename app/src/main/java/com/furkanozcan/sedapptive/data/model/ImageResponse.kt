package com.furkanozcan.sedapptive.data.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class ImageResponse(
    val small: String? = null,
    val large: String? = null
)
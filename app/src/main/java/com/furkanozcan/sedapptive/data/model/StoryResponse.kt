package com.furkanozcan.sedapptive.data.model

import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class StoryResponse(
    val name: String? = null,
    val category: String? = null,
    val image: ImageResponse? = null,
    val date: String? = null,
    val text: String? = null
)
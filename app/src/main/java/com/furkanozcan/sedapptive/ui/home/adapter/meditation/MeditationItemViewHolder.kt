package com.furkanozcan.sedapptive.ui.home.adapter.meditation

import androidx.recyclerview.widget.RecyclerView
import com.furkanozcan.sedapptive.databinding.ItemMeditationBinding
import com.furkanozcan.sedapptive.ui.model.MeditationUiModel
import com.furkanozcan.sedapptive.utils.load

class MeditationItemViewHolder(
    private val onItemClick: (title: String, content: String, releaseDate: String) -> Unit,
    private val binding: ItemMeditationBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: MeditationUiModel) {
        with(binding) {
            ivImage.load(item.largeImage)
            tvTitle.text = item.title
            tvSubTitle.text = item.subTitle
            itemView.setOnClickListener {
                onItemClick.invoke(item.title, item.content, item.releaseDate)
            }
        }
    }
}
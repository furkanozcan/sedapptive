package com.furkanozcan.sedapptive.ui

object DisplayItemType {
    const val MEDITATION_TYPE = 1
    const val STORY_TYPE = 2
    const val BANNER_TYPE = 3
    const val HEADER_TYPE = 4
}
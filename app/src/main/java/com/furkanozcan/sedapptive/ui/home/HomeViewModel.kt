package com.furkanozcan.sedapptive.ui.home

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.furkanozcan.sedapptive.data.HomeRepository
import com.furkanozcan.sedapptive.data.Resource
import com.furkanozcan.sedapptive.data.local.DataStorage
import com.furkanozcan.sedapptive.ui.DisplayItem
import com.furkanozcan.sedapptive.ui.home.adapter.banner.BannerDisplayItem
import com.furkanozcan.sedapptive.ui.home.adapter.header.HeaderDisplayItem
import com.furkanozcan.sedapptive.ui.home.adapter.meditation.MeditationDisplayItem
import com.furkanozcan.sedapptive.ui.home.adapter.story.StoryDisplayItem
import com.furkanozcan.sedapptive.ui.model.HomeUiModel
import com.furkanozcan.sedapptive.utils.EMPTY_STRING
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val homeRepository: HomeRepository,
    private val dataStorage: DataStorage
) : ViewModel() {

    private val _uiState = MutableSharedFlow<Resource<HomeUiModel>>(replay = 1)
    val uiState: SharedFlow<Resource<HomeUiModel>> = _uiState

    private val _userName = MutableStateFlow(EMPTY_STRING)
    val userName: StateFlow<String> = _userName

    init {
        getUserName()
        getHomeUiData()
    }

    private fun getHomeUiData() {
        viewModelScope.launch {
            homeRepository.getHomeUiData().collect { resource ->
                _uiState.emit(resource)
            }
        }
    }

    private fun getUserName() {
        viewModelScope.launch {
            dataStorage.getUserName().collect { userName ->
                _userName.value = userName
            }
        }
    }

    fun getDisplayItemList(
        homeUiModel: HomeUiModel?,
        meditationsHeaderTitle: String,
        storiesHeaderTitle: String
    ): List<DisplayItem> {
        val displayItems = mutableListOf<DisplayItem>()

        homeUiModel?.let { uiModel ->
            uiModel.meditations.takeIf { it.isNotEmpty() }?.let { meditations ->
                displayItems.add(
                    HeaderDisplayItem(meditationsHeaderTitle)
                )
                displayItems.add(
                    MeditationDisplayItem(meditations)
                )
            }

            if (uiModel.isBannerEnabled) {
                displayItems.add(
                    BannerDisplayItem(userName.value)
                )
            }

            uiModel.stories.takeIf { it.isNotEmpty() }?.let { stories ->
                displayItems.add(
                    HeaderDisplayItem(storiesHeaderTitle)
                )
                displayItems.add(
                    StoryDisplayItem(stories)
                )
            }
        }

        return displayItems
    }
}
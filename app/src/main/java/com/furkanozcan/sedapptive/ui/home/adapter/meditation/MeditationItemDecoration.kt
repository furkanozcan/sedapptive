package com.furkanozcan.sedapptive.ui.home.adapter.meditation

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class MeditationItemDecoration(
    private val marginLeft: Int,
    private val marginTop: Int,
    private val marginRight: Int
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        with(outRect) {
            when (parent.getChildAdapterPosition(view)) {
                0 -> {
                    left = marginLeft
                    right = marginRight
                    top = marginTop
                }
                else -> {
                    right = marginRight
                    top = marginTop
                }
            }
        }
    }
}
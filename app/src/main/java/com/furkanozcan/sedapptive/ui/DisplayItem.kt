package com.furkanozcan.sedapptive.ui

interface DisplayItem {
    fun type(): Int
}
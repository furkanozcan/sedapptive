package com.furkanozcan.sedapptive.ui.homedetail

import android.media.MediaPlayer
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.furkanozcan.sedapptive.R
import com.furkanozcan.sedapptive.base.BaseFragment
import com.furkanozcan.sedapptive.databinding.FragmentHomeDetailBinding
import com.furkanozcan.sedapptive.utils.orFalse

class HomeDetailFragment : BaseFragment<FragmentHomeDetailBinding>() {

    private val args: HomeDetailFragmentArgs by navArgs()

    private var mediaPlayer: MediaPlayer? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mediaPlayer = MediaPlayer().apply {
            setDataSource(getString(R.string.mp3_url))
            prepare()
        }

        with(binding) {
            toolbar.title = args.title
            tvTitle.text = args.title
            tvContent.text = args.content
            tvReleaseDate.text = args.releaseDate

            toolbar.setNavigationOnClickListener {
                findNavController().navigateUp()
            }

            binding.btnAction.setOnClickListener {
                val isPlaying = mediaPlayer?.isPlaying.orFalse()

                if (isPlaying) {
                    mediaPlayer?.pause()
                } else {
                    mediaPlayer?.start()
                }

                updateButtonBackground(!isPlaying)
            }
        }
    }

    private fun updateButtonBackground(isMusicPlaying: Boolean) {
        val backgroundDrawable = if (isMusicPlaying) {
            R.drawable.ic_pause
        } else {
            R.drawable.ic_play
        }

        binding.btnAction.setBackgroundResource(backgroundDrawable)
    }

    override fun setBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentHomeDetailBinding = FragmentHomeDetailBinding.inflate(inflater, container, false)

}
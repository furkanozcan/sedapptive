package com.furkanozcan.sedapptive.ui.home.adapter.banner

import com.furkanozcan.sedapptive.R
import com.furkanozcan.sedapptive.base.BaseViewHolder
import com.furkanozcan.sedapptive.databinding.LayoutBannerBinding
import com.furkanozcan.sedapptive.ui.DisplayItem

class BannerViewHolder(val binding: LayoutBannerBinding) : BaseViewHolder(binding.root) {
    override fun bind(item: DisplayItem) {
        if (item is BannerDisplayItem) {
            binding.tvText.text =
                String.format(itemView.context.getString(R.string.banner_text, item.userName))
        }
    }
}
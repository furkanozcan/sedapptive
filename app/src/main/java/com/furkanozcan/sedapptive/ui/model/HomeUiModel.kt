package com.furkanozcan.sedapptive.ui.model

data class HomeUiModel(
    val isBannerEnabled: Boolean,
    val meditations: List<MeditationUiModel>,
    val stories: List<StoryUiModel>
)
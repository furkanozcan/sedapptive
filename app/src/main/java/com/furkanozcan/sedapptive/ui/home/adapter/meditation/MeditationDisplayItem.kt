package com.furkanozcan.sedapptive.ui.home.adapter.meditation

import com.furkanozcan.sedapptive.ui.DisplayItem
import com.furkanozcan.sedapptive.ui.DisplayItemType.MEDITATION_TYPE
import com.furkanozcan.sedapptive.ui.model.MeditationUiModel

class MeditationDisplayItem(
    val meditationList: List<MeditationUiModel>
) : DisplayItem {
    override fun type(): Int = MEDITATION_TYPE
}
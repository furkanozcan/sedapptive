package com.furkanozcan.sedapptive.ui.home.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.furkanozcan.sedapptive.base.BaseViewHolder
import com.furkanozcan.sedapptive.databinding.LayoutBannerBinding
import com.furkanozcan.sedapptive.databinding.LayoutHeaderBinding
import com.furkanozcan.sedapptive.databinding.LayoutMeditiationBinding
import com.furkanozcan.sedapptive.databinding.LayoutStoryBinding
import com.furkanozcan.sedapptive.ui.DisplayItem
import com.furkanozcan.sedapptive.ui.DisplayItemType.HEADER_TYPE
import com.furkanozcan.sedapptive.ui.DisplayItemType.MEDITATION_TYPE
import com.furkanozcan.sedapptive.ui.DisplayItemType.STORY_TYPE
import com.furkanozcan.sedapptive.ui.home.adapter.banner.BannerViewHolder
import com.furkanozcan.sedapptive.ui.home.adapter.header.HeaderViewHolder
import com.furkanozcan.sedapptive.ui.home.adapter.meditation.MeditationViewHolder
import com.furkanozcan.sedapptive.ui.home.adapter.story.StoryViewHolder

class HomeAdapter(
    private val onItemClick: (title: String, content: String, releaseDate: String) -> Unit
) : ListAdapter<DisplayItem, BaseViewHolder>(HomeDiffUtil()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val inflater = LayoutInflater.from(parent.context)

        return when (viewType) {
            HEADER_TYPE -> {
                HeaderViewHolder(LayoutHeaderBinding.inflate(inflater, parent, false))
            }
            MEDITATION_TYPE -> {
                MeditationViewHolder(
                    onItemClick = onItemClick,
                    binding = LayoutMeditiationBinding.inflate(inflater, parent, false)
                )
            }
            STORY_TYPE -> {
                StoryViewHolder(
                    onItemClick = onItemClick,
                    binding = LayoutStoryBinding.inflate(inflater, parent, false)
                )
            }
            else -> BannerViewHolder(LayoutBannerBinding.inflate(inflater, parent, false))
        }
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    override fun getItemViewType(position: Int) = getItem(position).type()

}

class HomeDiffUtil : DiffUtil.ItemCallback<DisplayItem>() {
    override fun areItemsTheSame(oldItem: DisplayItem, newItem: DisplayItem): Boolean {
        return oldItem == newItem
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: DisplayItem, newItem: DisplayItem): Boolean {
        return oldItem == newItem
    }
}
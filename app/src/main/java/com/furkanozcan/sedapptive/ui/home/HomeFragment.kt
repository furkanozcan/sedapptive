package com.furkanozcan.sedapptive.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.furkanozcan.sedapptive.R
import com.furkanozcan.sedapptive.base.BaseFragment
import com.furkanozcan.sedapptive.data.Resource
import com.furkanozcan.sedapptive.databinding.FragmentHomeBinding
import com.furkanozcan.sedapptive.ui.home.adapter.HomeAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect

@AndroidEntryPoint
class HomeFragment : BaseFragment<FragmentHomeBinding>() {

    private val homeViewModel: HomeViewModel by viewModels()

    private var homeAdapter: HomeAdapter? = null

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setAdapter()
        observeData()
    }

    override fun setBinding(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentHomeBinding.inflate(inflater, container, false)

    private fun observeData() {
        lifecycleScope.launchWhenStarted {
            homeViewModel.uiState.collect { resource ->
                when (resource) {
                    is Resource.Success -> {
                        binding.progress.isVisible = false

                        homeAdapter?.submitList(
                            homeViewModel.getDisplayItemList(
                                homeUiModel = resource.data,
                                meditationsHeaderTitle = resources.getString(R.string.meditations_header_title),
                                storiesHeaderTitle = resources.getString(R.string.stories_header_title)
                            )
                        )
                    }
                    is Resource.Loading -> {
                        binding.progress.isVisible = true
                    }
                    is Resource.Error -> {
                        binding.progress.isVisible = false

                        resource.message?.let { showMessage(it) }
                    }
                }
            }
        }
    }

    private fun setAdapter() {
        homeAdapter = HomeAdapter(this@HomeFragment::onHomeItemClick).apply {
            binding.rvHomeList.adapter = this
        }
    }

    private fun onHomeItemClick(title: String, content: String, releaseDate: String) {
        findNavController().navigate(
            HomeFragmentDirections.navigateToHomeDetail(
                title = title,
                content = content,
                releaseDate = releaseDate
            )
        )
    }
}
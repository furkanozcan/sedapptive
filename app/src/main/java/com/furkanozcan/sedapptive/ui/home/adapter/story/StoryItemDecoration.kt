package com.furkanozcan.sedapptive.ui.home.adapter.story

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

class StoryItemDecoration(
    private val marginStart: Int,
    private val marginEnd: Int,
    private val marginTop: Int,
    private val marginBottom: Int,
    private val space: Int
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        with(outRect) {
            when (parent.getChildAdapterPosition(view) % 2) {
                0 -> {
                    left = marginStart
                    right = space / 2
                    top = marginTop
                    bottom = marginBottom
                }
                1 -> {
                    left = space / 2
                    right = marginEnd
                    top = marginTop
                    bottom = marginBottom
                }
            }
        }
    }
}
package com.furkanozcan.sedapptive.ui.home.adapter.story

import com.furkanozcan.sedapptive.ui.DisplayItem
import com.furkanozcan.sedapptive.ui.DisplayItemType.STORY_TYPE
import com.furkanozcan.sedapptive.ui.model.StoryUiModel

class StoryDisplayItem(
    val storyList: List<StoryUiModel>
): DisplayItem {
    override fun type(): Int = STORY_TYPE
}
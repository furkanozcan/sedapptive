package com.furkanozcan.sedapptive.ui.home.adapter.story

import androidx.recyclerview.widget.GridLayoutManager
import com.furkanozcan.sedapptive.R
import com.furkanozcan.sedapptive.base.BaseViewHolder
import com.furkanozcan.sedapptive.databinding.LayoutStoryBinding
import com.furkanozcan.sedapptive.ui.DisplayItem
import com.furkanozcan.sedapptive.ui.model.StoryUiModel

const val SPAN_COUNT = 2

class StoryViewHolder(
    private val onItemClick: (title: String, content: String, releaseDate: String) -> Unit,
    private val binding: LayoutStoryBinding
) : BaseViewHolder(binding.root) {

    override fun bind(item: DisplayItem) {
        if (item is StoryDisplayItem) {
            setRecyclerView(item.storyList)
        }
    }

    private fun setRecyclerView(item: List<StoryUiModel>) {
        with(binding.rvStory) {
            if (layoutManager == null) {
                layoutManager = GridLayoutManager(
                    itemView.context,
                    SPAN_COUNT
                )

                addItemDecoration(
                    StoryItemDecoration(
                        marginStart = itemView.context.resources.getDimensionPixelSize(R.dimen.story_item_start_margin),
                        marginEnd = itemView.context.resources.getDimensionPixelSize(R.dimen.story_item_end_margin),
                        marginTop = itemView.context.resources.getDimensionPixelSize(R.dimen.story_item_top_margin),
                        marginBottom = itemView.context.resources.getDimensionPixelSize(R.dimen.story_item_bottom_margin),
                        space = itemView.context.resources.getDimensionPixelSize(R.dimen.story_item_space_margin)
                    )
                )
            }

            adapter = StoryAdapter(onItemClick).apply {
                submitList(item)
            }
        }
    }
}
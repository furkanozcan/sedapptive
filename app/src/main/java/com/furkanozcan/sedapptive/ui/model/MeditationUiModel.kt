package com.furkanozcan.sedapptive.ui.model

class MeditationUiModel(
    val title: String,
    val subTitle: String,
    val smallImage: String,
    val largeImage: String,
    val releaseDate: String,
    val content: String
)
package com.furkanozcan.sedapptive.ui.model

class StoryUiModel(
    val name: String,
    val category: String,
    val smallImage: String,
    val largeImage: String,
    val date: String,
    val text: String
)
package com.furkanozcan.sedapptive.ui.home.adapter.meditation

import androidx.recyclerview.widget.LinearLayoutManager
import com.furkanozcan.sedapptive.R
import com.furkanozcan.sedapptive.base.BaseViewHolder
import com.furkanozcan.sedapptive.databinding.LayoutMeditiationBinding
import com.furkanozcan.sedapptive.ui.DisplayItem
import com.furkanozcan.sedapptive.ui.model.MeditationUiModel

class MeditationViewHolder(
    private val onItemClick: (title: String, content: String, releaseDate: String) -> Unit,
    private val binding: LayoutMeditiationBinding
) : BaseViewHolder(binding.root) {

    override fun bind(item: DisplayItem) {
        if (item is MeditationDisplayItem) {
            setRecyclerView(item.meditationList)
        }
    }

    private fun setRecyclerView(item: List<MeditationUiModel>) {
        with(binding.rvMeditation) {
            if (layoutManager == null) {
                layoutManager = LinearLayoutManager(
                    itemView.context,
                    LinearLayoutManager.HORIZONTAL,
                    false
                )

                addItemDecoration(
                    MeditationItemDecoration(
                        marginLeft = itemView.context.resources.getDimensionPixelSize(R.dimen.meditation_item_left_margin),
                        marginRight = itemView.context.resources.getDimensionPixelSize(R.dimen.meditation_item_right_margin),
                        marginTop = itemView.context.resources.getDimensionPixelSize(R.dimen.meditation_item_top_margin)
                    )
                )
            }

            adapter = MeditationAdapter(onItemClick).apply {
                submitList(item)
            }
        }
    }
}
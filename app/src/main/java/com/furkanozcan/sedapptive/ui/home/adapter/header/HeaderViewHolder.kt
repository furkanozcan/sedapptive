package com.furkanozcan.sedapptive.ui.home.adapter.header

import com.furkanozcan.sedapptive.base.BaseViewHolder
import com.furkanozcan.sedapptive.databinding.LayoutHeaderBinding
import com.furkanozcan.sedapptive.ui.DisplayItem

class HeaderViewHolder(
    private val binding: LayoutHeaderBinding
) : BaseViewHolder(binding.root) {

    override fun bind(item: DisplayItem) {
        if (item is HeaderDisplayItem) {
            binding.tvTitle.text = item.title
        }
    }
}
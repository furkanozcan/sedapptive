package com.furkanozcan.sedapptive.ui.home.adapter.banner

import com.furkanozcan.sedapptive.ui.DisplayItem
import com.furkanozcan.sedapptive.ui.DisplayItemType.BANNER_TYPE

class BannerDisplayItem(
    val userName: String
) : DisplayItem {
    override fun type(): Int = BANNER_TYPE
}
package com.furkanozcan.sedapptive.ui.home.adapter.story

import android.content.res.Resources
import androidx.recyclerview.widget.RecyclerView
import com.furkanozcan.sedapptive.R
import com.furkanozcan.sedapptive.databinding.ItemStoryBinding
import com.furkanozcan.sedapptive.ui.model.StoryUiModel
import com.furkanozcan.sedapptive.utils.load

class StoryItemViewHolder(
    private val onItemClick: (title: String, content: String, releaseDate: String) -> Unit,
    private val binding: ItemStoryBinding
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(item: StoryUiModel) {
        with(binding) {
            ivImage.run {
                layoutParams.run {
                    width = getImageSize()
                    height = getImageSize()
                }

                load(item.smallImage)
            }

            tvTitle.text = item.name
            tvSubTitle.text = item.text
            itemView.setOnClickListener {
                onItemClick.invoke(item.name, item.text, item.date)
            }
        }
    }

    private fun getImageSize(): Int {
        val marginStart =
            itemView.context.resources.getDimensionPixelSize(R.dimen.story_item_start_margin)
        val marginEnd =
            itemView.context.resources.getDimensionPixelSize(R.dimen.story_item_end_margin)
        val space =
            itemView.context.resources.getDimensionPixelSize(R.dimen.story_item_space_margin)

        return (getScreenWidth() - marginStart - marginEnd - space) / SPAN_COUNT
    }

    private fun getScreenWidth(): Int = Resources.getSystem().displayMetrics.widthPixels
}
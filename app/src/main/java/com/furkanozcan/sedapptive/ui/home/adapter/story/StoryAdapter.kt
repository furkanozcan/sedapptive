package com.furkanozcan.sedapptive.ui.home.adapter.story

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.furkanozcan.sedapptive.databinding.ItemStoryBinding
import com.furkanozcan.sedapptive.ui.model.StoryUiModel

class StoryAdapter(
    private val onItemClick: (title: String, content: String, releaseDate: String) -> Unit,
) : ListAdapter<StoryUiModel, StoryItemViewHolder>(StoryDiffUtil()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StoryItemViewHolder {
        return StoryItemViewHolder(
            onItemClick = onItemClick,
            binding = ItemStoryBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: StoryItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

}

class StoryDiffUtil : DiffUtil.ItemCallback<StoryUiModel>() {
    override fun areItemsTheSame(
        oldItem: StoryUiModel,
        newItem: StoryUiModel
    ): Boolean {
        return oldItem.name == newItem.name
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(
        oldItem: StoryUiModel,
        newItem: StoryUiModel
    ): Boolean {
        return oldItem == newItem
    }
}
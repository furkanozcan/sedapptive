package com.furkanozcan.sedapptive.ui.login

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.furkanozcan.sedapptive.data.local.DataStorage
import com.furkanozcan.sedapptive.utils.hasNumber
import com.furkanozcan.sedapptive.utils.hasUpperCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class LoginViewModel @Inject constructor(
    private val dataStorage: DataStorage,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {

    val userNameSavedState = savedStateHandle.getLiveData<String>(USER_NAME_STATE_KEY)
    val passwordSavedState = savedStateHandle.getLiveData<String>(PASSWORD_STATE_KEY)
    val continueButtonSavedState = savedStateHandle.getLiveData<Boolean>(BUTTON_STATE_KEY)

    fun login(userName: String) {
        viewModelScope.launch {
            dataStorage.setUserName(userName)
        }
    }

    fun isPasswordValid(password: String): Boolean = password.count() > 5 && password.hasNumber() &&
            password.hasUpperCase()

    fun isUserNameValid(userName: String): Boolean = userName.count() > 2

    fun saveUiState(
        userName: String,
        password: String,
        isButtonEnable: Boolean
    ) {
        savedStateHandle[USER_NAME_STATE_KEY] = userName
        savedStateHandle[PASSWORD_STATE_KEY] = password
        savedStateHandle[BUTTON_STATE_KEY] = isButtonEnable
    }

    companion object {
        private const val USER_NAME_STATE_KEY = "username"
        private const val PASSWORD_STATE_KEY = "password"
        private const val BUTTON_STATE_KEY = "isButtonEnable"
    }
}
package com.furkanozcan.sedapptive.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.furkanozcan.sedapptive.R
import com.furkanozcan.sedapptive.base.BaseFragment
import com.furkanozcan.sedapptive.databinding.FragmentLoginBinding
import com.furkanozcan.sedapptive.utils.EMPTY_STRING
import com.furkanozcan.sedapptive.utils.clearError
import com.furkanozcan.sedapptive.utils.isZero
import com.furkanozcan.sedapptive.utils.listenChanges
import com.furkanozcan.sedapptive.utils.orFalse
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LoginFragment : BaseFragment<FragmentLoginBinding>() {

    private val loginViewModel: LoginViewModel by viewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        handleUi()
        observeData()
    }

    override fun onDestroyView() {
        loginViewModel.saveUiState(
            userName = binding.userNameTextField.editText?.text.toString(),
            password = binding.passwordTextField.editText?.text.toString(),
            isButtonEnable = binding.btnContinue.isEnabled
        )
        super.onDestroyView()
    }

    override fun setBinding(inflater: LayoutInflater, container: ViewGroup?) =
        FragmentLoginBinding.inflate(inflater, container, false)

    private fun observeData() {
        loginViewModel.userNameSavedState.observe(viewLifecycleOwner) { userName ->
            binding.userNameTextField.editText?.setText(userName)
        }

        loginViewModel.passwordSavedState.observe(viewLifecycleOwner) { userName ->
            binding.passwordTextField.editText?.setText(userName)
        }

        loginViewModel.continueButtonSavedState.observe(viewLifecycleOwner) { isEnabled ->
            binding.btnContinue.isEnabled = isEnabled
        }
    }

    private fun handleUi() {
        with(binding) {
            btnContinue.setOnClickListener {
                loginViewModel.login(userNameTextField.editText?.text.toString())

                navigateToHome()
            }

            passwordTextField.run {
                editText?.listenChanges(
                    afterTextChangedListener = { text ->
                        text?.let { passwordText ->
                            if (passwordText.count().isZero()) {
                                clearError()
                                return@let
                            }

                            val password = passwordText.toString()

                            val errorText = if (!loginViewModel.isPasswordValid(password)) {
                                getString(R.string.password_error_text)
                            } else {
                                EMPTY_STRING
                            }

                            error = errorText
                            isErrorEnabled = errorText.isNotEmpty()

                            setContinueButtonEnabled()
                        }
                    }
                )
            }

            userNameTextField.run {
                editText?.listenChanges(
                    afterTextChangedListener = { text ->
                        text?.let { userNameText ->
                            if (userNameText.count().isZero()) {
                                clearError()
                                return@let
                            }

                            val errorText =
                                if (!loginViewModel.isUserNameValid(userNameText.toString())) {
                                    getString(R.string.user_name_error_text)
                                } else {
                                    EMPTY_STRING
                                }

                            error = errorText
                            isErrorEnabled = errorText.isNotEmpty()

                            setContinueButtonEnabled()
                        }
                    }
                )
            }
        }
    }

    private fun navigateToHome() {
        findNavController().navigate(LoginFragmentDirections.navigateToHome())
    }

    private fun setContinueButtonEnabled() {
        binding.btnContinue.isEnabled = !binding.userNameTextField.isErrorEnabled &&
                !binding.passwordTextField.isErrorEnabled &&
                binding.passwordTextField.editText?.text?.isNotEmpty().orFalse() &&
                binding.userNameTextField.editText?.text?.isNotEmpty().orFalse()
    }
}
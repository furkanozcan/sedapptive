package com.furkanozcan.sedapptive.ui.home.adapter.meditation

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.furkanozcan.sedapptive.databinding.ItemMeditationBinding
import com.furkanozcan.sedapptive.ui.model.MeditationUiModel

class MeditationAdapter(
    private val onItemClick: (title: String, content: String, releaseDate: String) -> Unit
) : ListAdapter<MeditationUiModel, MeditationItemViewHolder>(MeditationDiffUtil()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MeditationItemViewHolder {
        return MeditationItemViewHolder(
            onItemClick = onItemClick,
            binding = ItemMeditationBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    override fun onBindViewHolder(holder: MeditationItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

class MeditationDiffUtil : DiffUtil.ItemCallback<MeditationUiModel>() {
    override fun areItemsTheSame(
        oldItem: MeditationUiModel,
        newItem: MeditationUiModel
    ): Boolean {
        return oldItem.title == newItem.title
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(
        oldItem: MeditationUiModel,
        newItem: MeditationUiModel
    ): Boolean {
        return oldItem == newItem
    }
}
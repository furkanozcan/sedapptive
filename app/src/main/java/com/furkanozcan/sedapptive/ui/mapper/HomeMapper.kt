package com.furkanozcan.sedapptive.ui.mapper

import com.furkanozcan.sedapptive.data.Mapper
import com.furkanozcan.sedapptive.data.model.HomeResponse
import com.furkanozcan.sedapptive.ui.model.HomeUiModel
import com.furkanozcan.sedapptive.ui.model.MeditationUiModel
import com.furkanozcan.sedapptive.ui.model.StoryUiModel
import com.furkanozcan.sedapptive.utils.toHumanReadableDate
import javax.inject.Inject

class HomeMapper @Inject constructor() : Mapper<HomeResponse?, HomeUiModel> {
    override fun map(input: HomeResponse?): HomeUiModel {
        return HomeUiModel(
            isBannerEnabled = input?.isBannerEnabled ?: false,
            meditations = input?.meditations?.let { meditations ->
                meditations.map { meditation ->
                    val image = meditation.image

                    MeditationUiModel(
                        title = meditation.content.orEmpty(),
                        subTitle = meditation.subTitle.orEmpty(),
                        smallImage = image?.small.orEmpty(),
                        largeImage = image?.large.orEmpty(),
                        releaseDate = meditation.releaseDate.toHumanReadableDate(),
                        content = meditation.content.orEmpty()
                    )
                }
            } ?: emptyList(),
            stories = input?.stories?.let { stories ->
                stories.map { story ->
                    val image = story.image

                    StoryUiModel(
                        name = story.name.orEmpty(),
                        category = story.category.orEmpty(),
                        smallImage = image?.small.orEmpty(),
                        largeImage = image?.large.orEmpty(),
                        date = story.date.toHumanReadableDate(),
                        text = story.text.orEmpty()
                    )
                }
            } ?: emptyList()
        )
    }
}
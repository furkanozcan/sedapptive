package com.furkanozcan.sedapptive.ui.home.adapter.header

import com.furkanozcan.sedapptive.ui.DisplayItem
import com.furkanozcan.sedapptive.ui.DisplayItemType.HEADER_TYPE

class HeaderDisplayItem(
    val title: String
): DisplayItem {
    override fun type(): Int = HEADER_TYPE
}
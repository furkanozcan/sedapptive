package com.furkanozcan.sedapptive.base

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.furkanozcan.sedapptive.ui.DisplayItem

abstract class BaseViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    abstract fun bind(item: DisplayItem)
}
# Sedapptive

## Description

Sedapptive is a basic sample application based on modern Android application tech-stacks and MVVM architecture.

![previews](https://gitlab.com/furkanozcan/sedapptive/-/raw/master/previews/screenshot.png)

## Features

- Login screen
- Display dynamic list supporting different components
- Detail screen

## Tech stack & Open-source libraries

![api](https://img.shields.io/badge/API-23%2B-brightgreen.svg?style=flat)
- [AGP 7.0](https://developer.android.com/studio/releases/gradle-plugin#jdk-11) - JDK 11 required to run AGP 7.0
- [Kotlin](https://kotlinlang.org/) [Coroutines](https://github.com/Kotlin/kotlinx.coroutines) + [Flow](https://kotlin.github.io/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines.flow/)
- [Glide](https://github.com/bumptech/glide) - image loading
- [Hilt](https://dagger.dev/hilt/) - dependency injection.
- [Moshi](https://github.com/square/moshi/) - JSON library
- [Data Store](https://developer.android.com/topic/libraries/architecture/datastore) - local data storage
- [JetPack](https://developer.android.com/jetpack)
- [Architecture](https://developer.android.com/topic/libraries/architecture)
  - MVVM Architecture (View - ViewBinding - ViewModel - Model)
  - Repository pattern

[Guide to app architecture](https://developer.android.com/jetpack/guide)


![architecture](https://developer.android.com/topic/libraries/architecture/images/final-architecture.png)

## MAD Score
![mad score](https://gitlab.com/furkanozcan/sedapptive/-/raw/master/previews/summary.png)
